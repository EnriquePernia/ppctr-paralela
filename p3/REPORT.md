# P3: VideoTask
**1. Explica qu´e funcionalidad has tenido que a˜nadir/modificar sobre el c´odigo original.**

He paralelizado 
     #pragma omp parallel private(i)

     for (i = 0; i < seq; i++)

   {
      pixels[i] = (int *)malloc((height + 2) * (width + 2) * sizeof(int));

      filtered[i] = (int *)malloc((height + 2) * (width + 2) * sizeof(int));
   }

Y despues he creado tasks en el do while para las funciones fgauss y fwrite, que he dividido en dos bucles distintos. Al terminar el do while hago taskwait para que terminen ordenados.

     #pragma omp task 

        for(i=0;i<seq;i++){

         fgauss (pixels[i%seq], filtered[i%seq], height, width);

       }

        for(i=0;i<seq;i++){

         fwrite(filtered[i], (height+2) * (width + 2) * sizeof(int), 1, out);

       }


**2. Explica brevemente la funci´on que desarrolla cada una de las directivas y funciones de OpenMP que has utilizado, salvo las que ya hayas explicado en la practica anterior (por ejemplo parallel).**

Pragma omp task  genera tareas de forma explıcita.
Taskwait espera a que terminen todas las tareas, de esta forma lo sincronizamos.

**4. Explica c´omo se consigue el paralelismo en este programa (aplicable sobre la funci´on main).**

     #pragma omp for private(i)/
     #pragma omp task shared(i)/
     #pragma omp parallel/
     #pragma omp task 

     Estas son las directivas de paralelizacion utilizadas, he realizado paralelizacion de un bucle for mediante parallel for y despues he paralelizado el bucle do while mediante task de la forma vista en los apuntes.

**5. Calcula la ganancia (speedup) obtenido con la paralelizaci´on y explica los resultados obtenidos.**

Tiempo en paralelo : 0.560530;
Tiempo en secuencial: 2.011469;

Speedup = 3.54

Creso que es un buen speedup, razonable al haber paralelizado el main completo. Todas las partes del codigo en el main, que podian ser paralelizables, las hemos paralelizado por lo que se espera un buen speedup, ademas la carga de trabajo de las operaciones a realizar es grande al tener que filtrar imagenes, por lo que es muy notable la paralelizacion.

