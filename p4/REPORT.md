# P4: Mandelbrot
**2. Etiqueta todas las variables y explica por que le has asignado esa etiqueta.**

private(i, j, x, y, c) shared(n, count, r, g, b)

Las variables pirvadas son o los contadores de los bucles for, que no pueden ser sobreescritas por otros hilos, o variables locales que no pueden ser reasignadas antes de devolver su valor.
Las variables compartidas son las que no van a ser sobreescritas en ningun momento.

Sec section execution time p4: 0.767037
Paralel section execution time p4: 0.390748

Para la version paralelizada obtenemos un speedup de 1.96

**3. Explica brevemente como se realiza el paralelismo en este programa, indicando las partes que son secuenciales y las paralelas.**

 #pragma omp parallel for private(i, j, x, y) shared(n, count)

  for (i = 0; i < n; i++)

  {

    for (j = 0; j < n; j++)

    {

      x = ((double)(j)*x_max + (double)(n - j - 1) * x_min) / (double)(n - 1);

      y = ((double)(i)*y_max + (double)(n - i - 1) * y_min) / (double)(n - 1);

      count[i + j * n] = explode(x, y, count_max);
    }
  }

   c_max = 0;

 // #pragma omp parallel for private(i, j, c_max) shared(count, n) //Sin seccion critica

 //#pragma omp critical //Inicializa seccion critica

 omp_set_lock(&writelock); //Con directicvas runtime lock

  for (j = 0; j < n; j++)

  {

    for (i = 0; i < n; i++)

    {

      if (c_max < count[i + j * n])

      {

        c_max = count[i + j * n];

      }

    }

  }
   omp_unset_lock(&writelock); //Unlock

  /*
    Set the image data.
  */ 
  r = (int *)malloc(n * n * sizeof(int));
  g = (int *)malloc(n * n * sizeof(int));
  b = (int *)malloc(n * n * sizeof(int));
  

 #pragma omp parallel for private(i, j, c) shared(count, n, r, g, b)
  for (i = 0; i < n; i++)
  {
    for (j = 0; j < n; j++)
    {

      if (count[i + j * n] % 2 == 1)
      {
        r[i + j * n] = 255;
        g[i + j * n] = 255;
        b[i + j * n] = 255;
      }
      else
      {
        c = (int)(255.0 * sqrt(sqrt(sqrt(((double)(count[i + j * n]) /
                                          (double)(c_max))))));
        r[i + j * n] = 3 * c / 5;
        g[i + j * n] = 3 * c / 5;
        b[i + j * n] = c;
      }
    }
  }


Estas son las partes paralelas del programa, lo que he heho es buscar los bucles paralelizables y añadirles las directivas omp pertinentes.

#pragma omp parallel for private(i, j, c_max) shared(count, n) //Sin seccion critica
#pragma omp critical //Inicializa seccion critica
omp_set_lock(&writelock); //Con directicvas runtime lock

Este caso es el aplicado al realizar la siguiente parte de la practica, la implementacion mediante directivas omp, funciones de runtime y seccion critica.

**Realizar cuatro implementaciones distintas de la seccion crıtica y comprueba el rendimiento de cada una de ellas, calculando el speedup obtenido solo por ese bucle. Explica los resultados obtenidos apoyandote en graficas comparativas. Las cuatro implementaciones son las siguientes:**

Sec section execution time p4: 0.767037

Private section execution time p4: 0.758067 --> speedup: 1.012

Critical section execution time p4: 0.721896 --> speedup: 1.062

Runtime lock section execution time p4: 0.736409 --> speedup: 1.046