#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include <sys/times.h>

#define RAND rand() % 100

void init_mat_sup();
void init_mat_sup_sec();
void init_mat_inf();
void matmul();
void matmulSec();
void matmul_sup_sec();
void matmul_sup_static();
void matmul_sup_dynamic();
void matmul_sup_guided();
void matmul_inf();
void print_matrix(int dim, float *M); // TODO

/* Usage: ./matmul <dim> [block_size]*/
int main(int argc, char *argv[])
{
	printf("Max Threads: %d\n", omp_get_max_threads());

	int block_size = 1, dim;
	float *A, *B, *C;
	float time1;
	float parallelTime;
	dim = atoi(argv[1]);
	if (argc == 3)
		block_size = atoi(argv[2]);

	A = (float *)malloc(dim * dim * sizeof(float)); //Por que a veces da bien y a veces mal si pongo dim * 2 a partir de 5 
	B = (float *)malloc(dim * dim * sizeof(float));
	C = (float *)malloc(dim * dim * sizeof(float));
	init_mat_sup(dim, A);
	init_mat_sup(dim, B);
	init_mat_sup(dim, C);
	printf("Tamanho bloque: %d\n", block_size);
	time1 = omp_get_wtime();
	matmulSec(A, B, C, dim);
	parallelTime =  omp_get_wtime()-time1;
	printf("Sec section execution time p1: %f\n", parallelTime);
	time1 = omp_get_wtime();
	matmul(A, B, C, dim);
	parallelTime =  omp_get_wtime()-time1;
	print_matrix(dim, C);
	printf("Parallel section execution time p1: %f\n", parallelTime);
	time1 = omp_get_wtime();
	matmul_sup_sec(A, B, C, dim);
	float secTime =  omp_get_wtime()-time1;
	printf("Execution time p2_SEC: %f\n", secTime);
	time1 = omp_get_wtime();
	matmul_sup_static(A, B, C, dim, block_size);
	parallelTime =  omp_get_wtime()-time1;
	printf("Execution time p2_static: %f\n", parallelTime);
	time1 = omp_get_wtime();
	matmul_sup_dynamic(A, B, C, dim, block_size);
	parallelTime =  omp_get_wtime()-time1;
	printf("Execution time p2_dynamic: %f\n", parallelTime);
	time1 = omp_get_wtime();
	matmul_sup_guided(A, B, C, dim, block_size);
	parallelTime =  omp_get_wtime()-time1;
	printf("Execution time p2_guided: %f\n", parallelTime);
	exit(0);
}

void print_matrix(int dim, float *M)
{
	int i, j;

	for (i = 0; i < dim; i++)

		for (j = 0; j < dim; j++)
			i = i;
}

void init_mat_sup(int dim, float *M)
{
	int i, j, m, n, k;
	for (i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
			if (j <= i)
				M[i * dim + j] = 0.0;
			else
			{
				M[i * dim + j] = RAND;
			}
		}
	}
}

void init_mat_inf(int dim, float *M)
{
	int i, j, m, n, k;

	for (i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
			if (j >= i)
				M[i * dim + j] = 0.0;
			else
				M[i * dim + j] = RAND;
		}
	}
}

void matmulSec(float *A, float *B, float *C, int dim)
{
	int i, j, k;
	for (i = 0; i < dim; i++)
		for (j = 0; j < dim; j++)
			C[i * dim + j] = 0.0;

	for (i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
			for (k = 0; k < dim; k++)
			{
				C[i * dim + j] += A[i * dim + k] * B[j + k * dim];
			}
		}
	}
}

void matmul(float *A, float *B, float *C, int dim)
{
	int i = 0;
	int j = 0;
	int k = 0;
	omp_set_num_threads(omp_get_max_threads());
#pragma omp parallel for private(i, j) shared(C, dim) //Paralelizo el for, realizo compias de i y j para cada thread de forma que un thread no sobreescriba al otro, c y dim se comparten
	for (i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
			C[i * dim + j] = 0.0;
		}
	}
	omp_set_num_threads(omp_get_max_threads());
#pragma omp parallel for private(i, j, k) shared(C, A, B, dim)
	for (i = 0; i < dim; i++)
	{
		for (j = 0; j < dim; j++)
		{
			for (k = 0; k < dim; k++)
			{
				C[i * dim + j] += A[i * dim + k] * B[j + k * dim];
			}
		}
	}
}

void matmul_sup_static(float *A, float *B, float *C, int dim, int blockSize)
{
	int i, j, k;
	
#pragma omp parallel for private(i, j) shared(C, dim) schedule(static)
	for (i = 0; i < dim; i++)
		for (j = 0; j < dim; j++)
			C[i * dim + j] = 0.0;
#pragma omp parallel for private(i, j, k) shared(C, A, B, dim) schedule(static)
	for (i = 0; i < (dim - 1); i++)
		for (j = 0; j < (dim - 1); j++)
			for (k = (i + 1); k < dim; k++)
				C[i * dim + j] += A[i * dim + k] * B[j + k * dim];
}

void matmul_sup_dynamic(float *A, float *B, float *C, int dim, int blockSize)
{
	int i, j, k;
	
#pragma omp parallel for private(i, j) shared(C, dim) schedule(dynamic,blockSize)
	for (i = 0; i < dim; i++)
		for (j = 0; j < dim; j++)
			C[i * dim + j] = 0.0;
#pragma omp parallel for private(i, j, k) shared(C, A, B, dim) schedule(dynamic,blockSize)
	for (i = 0; i < (dim - 1); i++)
		for (j = 0; j < (dim - 1); j++)
			for (k = (i + 1); k < dim; k++)
				C[i * dim + j] += A[i * dim + k] * B[j + k * dim];
}

void matmul_sup_guided(float *A, float *B, float *C, int dim, int blockSize)
{
	int i, j, k;
	
#pragma omp parallel for private(i, j) shared(C, dim) schedule(guided)
	for (i = 0; i < dim; i++)
		for (j = 0; j < dim; j++)
			C[i * dim + j] = 0.0;
#pragma omp parallel for private(i, j, k) shared(C, A, B, dim) schedule(guided)
	for (i = 0; i < (dim - 1); i++)
		for (j = 0; j < (dim - 1); j++)
			for (k = (i + 1); k < dim; k++)
				C[i * dim + j] += A[i * dim + k] * B[j + k * dim];
}

void matmul_sup_sec(float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i = 0; i < dim; i++)
		for (j = 0; j < dim; j++)
			C[i * dim + j] = 0.0;

	for (i = 0; i < (dim - 1); i++)
		for (j = 0; j < (dim - 1); j++)
			for (k = (i + 1); k < dim; k++)
				C[i * dim + j] += A[i * dim + k] * B[j + k * dim];
}

void matmul_inf(float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i = 0; i < dim; i++)
		for (j = 0; j < dim; j++)
			C[i * dim + j] = 0.0;

	for (i = 1; i < dim; i++)
		for (j = 1; j < dim; j++)
			for (k = 0; k < i; k++)
				C[i * dim + j] += A[i * dim + k] * B[j + k * dim];
}
