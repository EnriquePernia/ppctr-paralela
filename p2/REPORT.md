# P2: MatMul


**1. Explica brevemente la función que desarrolla cada una de las directivas y funciones de OpenMP que has utilizado.**

     `omp_set_num_threads(omp_get_max_threads());`
     En esta linea `omp_get_max_threads()` nos indica el numero maximo de threads para nuestro equipo, y con la funcion `omp_set_num_threads` asignamos una cantidad de threads a ejecutar.

     `float time1 = omp_get_wtime();`
     En esta linea `omp_get_wtime()` nos indica el tiempo en un instante dado, esto lo utilizamos para contar el tiempo de ejecución, obteniendo el tiempo antes y después de ejecutar el programa.

**2.Etiqueta (OpenMP) todas las variables y explica por qué le has asignado esa etiqueta.** 

     Las variables privadas se utilizan para que los hilos que utilizan las mismas variables no las sobreescriban los unos a los otros, asi evitamos errores.
     En cambio, las variables compartidas no se modifican en los threads por lo que no necesitamos hacerlas privadas

     `#pragma omp parallel for private(i, j) shared(C, dim)`
          Este conjunto de directivas lo utilizamos para paralelizar el bucle for, haciendo privadas las variables i y j de cada hilo y compartidas las variables C y dim.

     `#pragma omp parallel for private(i, j, k) shared(C, A, B, dim)`
          Este conjunto de directivas lo utilizamos para paralelizar el bucle for, haciendo privadas las variables k, i y j de cada hilo y compartidas las variables C, A, B y dim.    

     `#pragma omp parallel for private(i, j, k) shared(C, A, B, dim) schedule(dynamic, blockSize)`
          Este conjunto de directivas lo utilizamos para paralelizar el bucle for, haciendo privadas las variables k, i y j de cada hilo y compartidas las variables C, A, B y dim. Hacemos una planificacion dinamica de los hilos. 

     `#pragma omp parallel for private(i, j, k) shared(C, A, B, dim) schedule(guided, blockSize)`
           Este conjunto de directivas lo utilizamos para paralelizar el bucle for, haciendo privadas las variables k, i y j de cada hilo y compartidas las variables C, A, B y dim. Hacemos una planificacion guiada de los hilos. 

     `#pragma omp parallel for private(i, j, k) shared(C, A, B, dim) schedule(static, blockSize)`
           Este conjunto de directivas lo utilizamos para paralelizar el bucle for, haciendo privadas las variables k, i y j de cada hilo y compartidas las variables C, A, B y dim. Hacemos una planificacion estatica de los hilos. 

**3. Explica cómo se reparte el trabajo entre los threads del equipo en el código paralelo.**

     En el codigo paralelo, las variables privadas se reparten de forma equitativa, por ejemplo Thread 0: i=0,1,2,3,. . . ,49
     Thread 1: i=50, 51, 52, . . . 99

**4. Calcula la ganancia (speedup) obtenido con la paralelización.**

Max Threads: 8

Sec section execution time p1: 5.023469

Parallel section execution time p1: 1.458039

Execution time p2_SEC: 2.323761

Execution time p2_static: 0.734513

Execution time p2_dynamic: 0.615560

Execution time p2_guided: 0.786688

Siendo estos los resutados obtenidos en la ejecucion del codigo, la ganancia es:

     Para matmul : 3.445

     Para p2_static: 3.161

     Para p2_dynamic: 3.771

     Para p2_guided: 2.952

**1. ¿Qué diferencias observas con el ejercicio anterior en cuanto a la ganancia obtenida sin la clausula schedule? Explica con detalle a qué se debe esta diferencia.**

     El tiempo de ejecucion respecto a matmul se ve muy reducido, ya que en este caso lo que estamos haciendo en gran parte es multiplicar ceros, que es mucho menos costoso que multiplicar numeros.
     

**2. ¿Cuál de los tres algoritmos de equilibrio de carga obtiene mejor resultado? Explica por qué ocurre esto, en función de cómo se reparte la carga de trabajo y las operaciones que tiene que realizar cada thread.**

     El mayor speedup lo obtiene el dynamic ya que las iteraciones se dividen en bloques del tamano especificado y se asignan dinamicamente a los threads cuando van acabando su trabajo. De forma que si un thread esta multiplicando ceros y otro esta realizando una operacion que requiera menos tiempo, este ultimo realizara mas operaciones.

**3. Para el algoritmo static piensa cuál será el tamaño del bloque óptimo, en función de cómo se reparte la carga de trabajo.**

     A la conclusion que he llegado en este apartado es que yo crearia bloques en funcion del numero de threads que tenga, si como en mi caso tengo 8 threads, dividiria el tamaño total entre 8 y asi obtendria el tamaño de bloque. Como he explicado en el apartado anterior, algunos threads tendran mas carga que otros y por ello, una planificacion dinamica seria mas eficiente.

**4. Para el algoritmo dynamic determina experimentalmente el tamaño del bloque óptimo. Para ello mide el tiempo de respuesta y speedup obtenidos para al menos 10 tamaños de bloque diferentes. Presenta una tabla resumen de los resultados y explicar detalladamente estos resultados.**

Execution time p2_dynamic 1: 0.612354

Execution time p2_dynamic 10: 0.619292

Execution time p2_dynamic 20: 0.625924

Execution time p2_dynamic 30: 0.633799

Execution time p2_dynamic 40: 0.637249

Execution time p2_dynamic 50: 0.641745

Execution time p2_dynamic 60: 0.633493

Execution time p2_dynamic 70: 0.659024

Execution time p2_dynamic 80: 0.638461

Execution time p2_dynamic 90: 0.689733

Execution time p2_dynamic 100: 0.693180

 ----------
 
Execution time p2_dynamic 1: 0.612012

Execution time p2_dynamic 10: 0.610295

Execution time p2_dynamic 20: 0.621815

Execution time p2_dynamic 30: 0.642669

Execution time p2_dynamic 40: 0.649721

Execution time p2_dynamic 50: 0.654160

Execution time p2_dynamic 60: 0.654585

Execution time p2_dynamic 70: 0.680783

Execution time p2_dynamic 80: 0.651637

Execution time p2_dynamic 90: 0.713281

Execution time p2_dynamic 100: 0.703138

 ----------
 
Estos son los resutados obtenidos al ejecutar 2 veces ./matmul 1000 1,10,20,30,40,50,60,70,80,90,100 en ellos vemos una tendencia a mejorar el tiempo de ejecucion al utilizar bloques pequeños respecto al uso de bloques de mayor tamaño, por tanto si tuviese que escoger un tamaño de bloque para este caso, probablemente seria 10.

**5. Explica los resultados del algoritmo guided, en función del reparto de carga de trabajo que realiza.**

Al asignar esta directiva, el tamaño de bloques es decreciente, tardará mas que la dinámica ya que para los bloques de trabajo mas grandes en algunos casos le llevara mucho tiempo si resulta que tiene que realizar varias operaciones costosas.

**1. matmul_inf : ¿Cómo se comportará? Compáralo con el ejercicio anterior.**

Creo que se comportará igual que la funcion matmul_sup, ya que la operacion es similar.

**2. matmul_inf  : ¿Cuál crees que es el mejor algoritmo de equilibrio en este caso? Explica las diferencias que ves con el ejercicio anterior.**

Las diferencias que veo es que en este caso, cuando avanzan las iteraciones, los bucles interiores son mas costosos, en cambio en matmul_sup es al revés.
Creo que una planificacion dinámica seria la mejor opción.

**3. matmul_inf  : ¿Cuál es el peor algoritmo para este caso?**

El peor algoritmo sera el static ya que tenemos el mismo problema que antes, algunos threads tendran mas carga que otros y por tanto el trabajo no se repartira de forma equitativa.