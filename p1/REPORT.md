# P1: Multithreading en C++
**1. Indica el tiempo que has necesitado para realizar la práctica y el tiempo invertido en cada una de las secciones de la rúbrica.**

Para esta practica he invertido alrededor de 17 horas, todo lo que he hecho me ha llevado mucho tiempo, nada me ha parecido sencillo.

**2. Indica qué has conseguido y qué te falta por cada apartado de la rúbrica. Además, describe las dificultades encontradas durante dicho proceso.**

Lo ultimo que que he implementado ha sido el logger, me falta el conector y el registro de finalizacion.
Dificultades he encontrado muchas, al no saber apenas c/c++ he tenido qeu estar constantemente mirando en internet e intentado qeu las cosas funcionaran como fuera, la refactorizacion ha sido lo mas sencillo, siendo la practica en general bastante dura.

**3. Explica el funcionamiento del programa con mayor detalle que lo indicado en el enunciado, haciendo especial hincapié en el paralelismo y las sincronizaciones.**

El programa recibe como parametros  el número de elementos (arraylength) y la operación a efectuar ((sum|xor)) y además, admite dos modos de funcionamiento, single thread o multi thread.
Se trata el array dividiendolo segun el numero de threads a utilizar, poniendo el trabajo extra, en caso de que lo haya, al primer subarray.
Las operaciones a realizar pueden ser sum o xor, aque se realizara el trabajo para cada subarray y despues se juntaran y se mostraran en el main.
He dividido el programa en dos archivos para tenerlo mas organizado, uno en el que se encuntra la funcion que trata los arrays(p1.cpp) y otro con el main.
El paralelismo lo realizamos con la libreria thread, creamos los threads de esta manera std::thread(&solveArray::doOp, solve, std::ref(array), opType, start, end); lo que quiere decir queese thread va a llamar a la funcion solveArray inicializada en el main como "solve" y que va a recibir como argumentos el array a tratar, la operacion a realizar(Que es un atributo del tipo enum definido en p1.cpp) y las posiciones del array que debe abarcar.
Despues el loger 
int logger(int *previousValue) //Le pasamos un puntero a la direccion de la variable x donde iremos almacenando los valores
{

  while (conditionalLogger != 2)
  {

    if (conditionalLogger == 1)

    {
      *previousValue += loggerValue;

      std::cout << "logger : " << *previousValue << '\n';

      conditionalLogger = 0; //Volvemos a esperar otro resultado
    }
  }

  return 0;
}
se encarga de ir mostrando los resultados, funciona mediante el uso de una variable que se llama conditionalLogger, que va coambiando sus valores para activar/esperar/desactivar a medida que van terminando los hilos.


**4.Explica el particionado aplicado, qué variaciones se te ocurren y posibles efectos.**

Para dividir el array, primero compruebo si el modulo de la division de la longitud del array entre el numero de threads es cero, en ese caso, la longitud de los subarray sera el resultado de la division. En caso contrario, 

if ((num % numThreads) != 0)

  { //Para tener en cuenta los  numeros que faltan(Trabajo extra para el thread 1)

    extraNumbers = num - floor(num / numThreads) * numThreads;

  };                                                  //Para saber la longitud de los arrays

  int size0 = floor(num / numThreads) + extraNumbers; //Tamanho del array para primer thread

  int size1 = floor(num / numThreads);                //Tamanho del array para resto de threads
  
  realizo esta operacion, en la que se asigna el trabajo extra al primer subarray, que tendra ongitud size0.

**5. (Base) Explica cómo has hecho los benchmarks, qué metodología de medición has usado (ROI+gettimeofday, externo), proporciona los resultados y haz un análisis de los mismos.**

Longitud del array : 1000000000;

Sum:

     thread -->1 :  Elapsed time for algorithm sum: 12650916

                    Sum: 852516346 

     thread -->2 :  Elapsed time for algorithm sum: 5399094

                    Sum: 852516346

     thread -->3 :  Elapsed time for algorithm sum: 5325439

                    Sum: 852516346

     thread -->4 :  Elapsed time for algorithm sum: 5318971

                    Sum: 852516346

     thread -->5 :  Elapsed time for algorithm sum: 5325489

                    Sum: 852516346

     thread -->6 :  Elapsed time for algorithm sum: 5325638

                    Sum: 852516346

     thread -->7 :  Elapsed time for algorithm sum: 5345392

                    Sum: 852516346

     thread -->8 :  Elapsed time for algorithm sum: 5315701

                    Sum: 852516346

Los benchmarks los he realizado ejecutando el codigo con distintos valores para los threads,como vemos los resultados varian bastante de utilizar un thread a utilizar 2, pero en cambio no se aprecian cambios notables al utilizar mas de dos, esto es porque la suma es una operacion muy sencilla, que no requiere apenas potencia de calculo, si la operacion fuese otra, seguramente se notaria el cambio de utilizar mas de dos threads. Tambien si el array fuera mucho mas largo.

**7. (Mantenibilidad)**
Para mejorar la mantenibilidad he separado el programa en varios archivos, he refactorizado la funcion que opera con el array ya que al principio tenia dos funciones separadas, he creado un enum para diferenciar los tipos de operaciones ya que antes lo hacia con strings. Antes le pasaba a la funcion que resuelve el array los distintos subarrays, ahora lo he cambiado para solo tener que pasarle el array completo, asi no tengo qeu crear los subarrays.
He cambiado muchos ifs por switch, asi es mas facil de leer. Todas las variables tienen nombres muy descriptivos de forma que es muy facil saber su utilidad.